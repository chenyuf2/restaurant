import React, { Component } from 'react';
import '../components/css/introduction.css';


export default class Introduction extends Component {
    render() {
        return (
            <div>
                <div className="container">
                    <div className="sub-title-container mb-5">
                    <h2 className="sub-title">What We Provide</h2>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 col-md-6 col-lg-6">
                            <img className="service-img" src={require('../temp1.png')}></img>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-6 flex-container">
                            <div className="service-block">
                                <h3 className="service-block-title">Lorem ipsum</h3>
                                <p className="service-block-subtitle">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vitae reiciendis non, pariatur distinctio at ea commodi vel itaque, ullam inventore iure voluptatum sapiente. Possimus sequi eligendi, deleniti dicta dolorem optio?</p>
                                <button className="btn btn-lg service-btn mt-2">Explore</button>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col-sm-6 col-md-6 col-lg-6 flex-container">
                            <div className="service-block">
                                <h3 className="service-block-title">Lorem ipsum</h3>
                                <p className="service-block-subtitle">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vitae reiciendis non, pariatur distinctio at ea commodi vel itaque, ullam inventore iure voluptatum sapiente. Possimus sequi eligendi, deleniti dicta dolorem optio?</p>
                                <button className="btn btn-lg service-btn mt-2">Explore</button>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-6">
                            <img className="service-img" src={require('../temp2.png')}></img>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col-sm-6 col-md-6 col-lg-6">
                            <img className="service-img" src={require('../temp3.png')}></img>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-6 flex-container">
                            <div className="service-block">
                                <h3 className="service-block-title">Lorem ipsum</h3>
                                <p className="service-block-subtitle">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vitae reiciendis non, pariatur distinctio at ea commodi vel itaque, ullam inventore iure voluptatum sapiente. Possimus sequi eligendi, deleniti dicta dolorem optio?</p>
                                <button className="btn btn-lg service-btn mt-2">Explore</button>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        )
    }
}