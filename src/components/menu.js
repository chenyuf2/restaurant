import React, { Component } from 'react';
import './css/menu.css';
import Navigation from './navigation';


export default class Menu extends Component {
    render() {
        return (
            <div className="mt-5" id="menu">
                <h2 className="sub-title mb-5">What We Serve This Week</h2>
                <div className="menu-img-container">
                    <img className="menu-img" src={require('../Abract01.png')}></img>
                </div>
                <div className="container" style={{marginBottom: '14rem', marginTop: "6rem"}}>
                    <div className="row">
                        <div className="col-sm-4 col-md-4 col-lg-4" style={{paddingRight: '2rem', paddingLeft: '2rem'}} id="card1">
                            <div class="card">
                            <img class="card-img-top" src="https://imagesvc.meredithcorp.io/v3/mm/image?q=85&c=sc&poi=face&url=https%3A%2F%2Fimg1.cookinglight.timeinc.net%2Fsites%2Fdefault%2Ffiles%2Fstyles%2F4_3_horizontal_-_1200x900%2Fpublic%2Fimage%2F2015%2F10%2Fmain%2F1511p187-chocolate-chip-cream-puffs_0_0.jpg%3Fitok%3DSJuWabmL" alt="Card image cap"></img>
                            <div class="card-body">
                                <p class="card-text mt-4 mb-4">Some quick example text to build on the card title </p>
                                <a href="#" class="btn btn-primary">Go somewhere</a>
                            </div>
                            </div>
                        </div>
                        <div className="col-sm-4 col-md-4 col-lg-4" style={{paddingRight: '2rem', paddingLeft: '2rem'}} id="card2">
                            <div class="card">
                                <img class="card-img-top" src="https://m.economictimes.com/thumb/msid-72299767,width-1200,height-900,resizemode-4,imgsize-365179/indian-food-bccl.jpg" alt="Card image cap"></img>
                                <div class="card-body">
                                    <p class="card-text mt-4 mb-4">Some quick example text to build on the card title </p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4 col-md-4 col-lg-4" style={{paddingRight: '2rem', paddingLeft: '2rem'}} id="card3">
                            <div class="card">
                                <img class="card-img-top" src="https://cdn.vox-cdn.com/thumbor/EAT9lhEIgUfXJKR_S0ohvxzFQdE=/0x0:2000x1335/1200x900/filters:focal(840x508:1160x828)/cdn.vox-cdn.com/uploads/chorus_image/image/55947063/2019_09_25_YangsKitchen_025.18.jpg" alt="Card image cap"></img>
                                <div class="card-body">
                                    <p class="card-text mt-4 mb-4">Some quick example text to build on the card title</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}