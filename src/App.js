import React, { Component } from 'react';
import Home from './components/home';
import Navigation from './components/navigation';
import Introduction from './components/introduction';
import Block from './components/block';
import Menu from './components/menu';
import Footer from './components/footer';
import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
  Link,
} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <BrowserRouter basename = '/restaurant'>
        <Switch>
      <Route exact path="/">
      <Home></Home>
      <Introduction></Introduction>
      <Block></Block>
      <Menu></Menu>
      <Footer></Footer>
      </Route>
      </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
